\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Expectativas Racionais e a Cr\IeC {\'\i }tica de Lucas}{3}{0}{1}
\beamer@sectionintoc {2}{Modelos DSGE: Din\IeC {\^a}micos, Estoc\IeC {\'a}sticos e de Equil\IeC {\'\i }brio Geral}{11}{0}{2}
\beamer@sectionintoc {3}{Uma Estima\IeC {\c c}\IeC {\~a}o Emp\IeC {\'\i }rica e seu uso para a Pol\IeC {\'\i }tica Econ\IeC {\^o}mica}{28}{0}{3}
