\documentclass{article}

\usepackage{breqn}
\usepackage{ragged2e}

\renewcommand{\thesection}{\Roman{section}} % Header's counters in roman numbers

\usepackage{Sweave}
\begin{document}
\input{ades1997_summary-concordance}

\title{Summary of Ades and Di Tella's paper (1997)}
\author{Marcelo Gelati, \\
  Rio Grande do Sul, \\
  Brazil \\}
\date{\today}
\maketitle % Necessary to create the top matter

\section{Introduction}

In this paper the authors' main objective is to capture the side effects of a pretended industrial policy. Mainly, they want to verify if the positive effects are significantly slowed down in the presence of corruption. The rent-seeking process in a society is verified by a theoretical model. After that, they collect data and run statistical tests to test their hypothesis.

The main conclusion of this research is that corruption \emph{must} be considered in the cost-benefit analysis of industrial policy.

\section{Modelling}

The variables in the model are:

\begin{itemize}
\item $e$: investment in cost reducing activities
\item $w$: the wage cost per unit of investment
\item $c(e)$: the variable cost
\item $\theta$: the firm's revenues
\item $n$: the number of firms
\item $\delta$: the discount factor
\item $\lambda$: the subsidy paid for every scientist
\item $q$: probability of being detected in the scheme
\item $f(g)$: fine for being detected
\item $g$: proportion of firms asked for bribes
\end{itemize}

Besides that, we have some properties for our functions:

\begin{eqnarray}
c'(e) < 0 \quad \mathrm{and} \quad  c''(e) > 0 \\
f'(g) > 0 \quad \mathrm{and} \quad f''(g) > 0
\end{eqnarray}

Let's get the grasp of the first equation. Quoting the original paper, ``\ldots in the first period, firms must incur a sunk cost by hiring scientists to undertake research and development at a wage cost $w$ [\ldots]. In the second period, production is undertaken at variable cost $c(.)$. Then, investment is given by the solution to the following problem solved by each of the $n$ firms (with $n$ large) in the economy \ldots"

\begin{eqnarray}
max_e \; - we + \delta[\theta - c(e)]
\end{eqnarray}

The optimal investment is then given by

\begin{eqnarray}
-w - \delta c'(e^*) = 0
\end{eqnarray}

Now let's add a subsidy for each scientist hired and look at the optimal investment

\begin{eqnarray}
-w + \delta [-c'(e^{FB}) + \lambda] = 0
\end{eqnarray}

We're adding corruption now. There is some bureaucrat who can confiscate the firm's second-period profits and he wants to maximize the number of firms he asks for bribe. His expected income is given by

\begin{eqnarray} \label{bureau}
max_g \; (1 - q) \, g \, [\theta - c(e) + \lambda e] - qf(g)
\end{eqnarray}

Equation \ref{bureau} tells us that he can confiscate all $g$ firms' profits with $(1 - q)$ probability or be fined by $f(g)$ with $q$ probability. 

The FPO is

\begin{eqnarray}
(1 - q) \, [\theta - c(e) + \lambda e] - qf'(g)
\end{eqnarray}

Now the firms have another investment decision problem. Noticing that $g$ is the probability of bribe demands ocurring, firm's decision is now

\begin{eqnarray}
max_e \; -we + \delta \, (1-g) \, [\theta - c(e) + \lambda e] \\
g = \mathrm{argmax} \, (1-q) \, g[\theta - c(e) + \lambda e] - qf(g) 
\end{eqnarray}

So, we have a new FPO for the firms

\begin{eqnarray}
-w + \delta \, (1-g) \, [-c'(e) + \lambda] - \frac{\partial g}{\partial e} \, \delta \, \pi \\
\mathrm{where} \quad \pi = \theta - c(e) + \lambda e \nonumber
\end{eqnarray}

We want to know the sign of $\displaystyle \frac{\partial e}{\partial \lambda}$, because it is the effect of subsidies in investment. Naturally, the industrial policy effect in investment is lower when we have corruption in our model. So, let's talk about this derivative. 

\texttt{CASE 1}: $\displaystyle \frac{\partial e}{\partial \lambda} > 0$. In this situation, corruption is responsible for a sub-optimal industrial policy. That is, you need \emph{more} subsidies in a corruption environment to reach the optimal policy compared to a non-corruption environment. The authors called this situation \emph{Super-Pigouvian Industrial Policy}. 

\texttt{CASE 2}: $\displaystyle \frac{\partial e}{\partial \lambda} < 0$. In this peculiar case, industrial policy actually \emph{deters} investment. That is because the negative effect is so high in the presence of corruption, that subsidies are a bad policy. Some elements for this situation might be low probability of being caught, low fine when being caught\ldots So, a tax may be called for in this situation. The authors named this \emph{Anti-Pigouvian Industrial Policy}.

\section{The empirical approach}

The paper tries to estimate the relation between corruption and intensity of industrial policy, controlling for other variables. The equation is

\begin{dmath}
CORR_i = \beta_0 + \beta_1 GDP_i + \beta_2 SCHOOL_i + \beta_3 POL_i + \beta_4 SECUR_i + \beta_5 OPEN_i + \beta_6 \lambda_i + \epsilon_i
\end{dmath}

And the equation's variables are

\begin{itemize}
\item the subscript $i$: countries
\item $CORR$: measure of corruption
\item $\lambda$: intensity of industrial policy
\item $GDP$: income per capita
\item $SCHOOL$: average years of schooling
\item $POL$: political rights
\item $SECUR$: crime prevention
\item $OPEN$: imports over GDP
\end{itemize}

The data selected by the authors comes mainly from the \emph{World Competitiveness Report (WCR)}. The objective is to measure $\beta_6$. In the authors' words, ``We use two indices of industrial policy from the \emph{WCR} survey section. A procurement index (\emph{PROCUR}) that measures `the extent to which public procurement is open to foreign bidders', and a fiscal index (\emph{FISCAL}) that measures `the extent to which there is equal fiscal treatment to all enterprises'". The indices goes from 0 to 100, with 100 meaning a more intense ``industrial policy" (captured by the authors' variables).

Besides these two indicators, they use another two. One is \emph{SUBSID89}, that represents monetary subsidies to private and public enterprises. The other indicator is \emph{SUPPM87}, the support to manufacturing as a percentage of sectoral GDP.

\section{The empirical evidence}

The first regressions shows that neither $GDP$ or $POL$ affects significantly the level of corruption. $SCHOOL$, $SECUR$
and $OPEN$, on the other hand, affects corruption \emph{negatively} and significantly. 
 
When we introduce industrial policy, with the $PROCUR$ variable, we notice that there is a correlation between it and level of corruption. The same occurs with the $FISCAL$ variable. The results still apply using the other corruption index, and when using $SUPPM87$ and $SUBSID89$ too.

The authors runs another regression to verify how much industrial policy and high investiment are associated. They verify that they are significantly and \emph{positively} associated. Together with the previous results, they infer that corruption \emph{reduces} investment.

\section{Conclusion}

A huge part of the benefits of industrial policies are shattered down in the presence of corruption. They conclude that high levels of active industrial policy must be taken in consideration very carefully, as it fosters corruption. They notice that, in the presence of corruption, ``the total effect of industrial policy on investment ranges between 84\% and 56\% of the direct impact". Based on the theoretical model, the optimal subsidy seems to be the \emph{Super-Pigouvian} type.

Again, the main conclusion is that, when designing a market intervention policy, corruption must be taken into consideration because of its potential costs on the policy itself  .

\end{document}
