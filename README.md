# README #

### What is this repository for? ###

This is my repository for research at Federal University of Rio Grande do Sul (UFRGS). Currently, I'm oriented by professor 
Marcelo de Carvalho Griebeler to research on economics. My central research interest is Game Theory, Behavioral Economics and
Mechanism Design. However, because of the limitations at UFRGS, I'll be mainly helping research at Applied Theory and Mathematical Economics.

Everything related to my research will be stored here. Everything, from the papers I read to the code I write, will be maintained here for the sake of organization.

### Structure of the repository ###

The repository will be organized in this way:

* Papers and their summaries
* Textbooks and their summaries 
* My papers
* My R codes

This is just a scratch. It will get more robust as the research goes on.

### Languages ###

I'll write every PDF file in LaTeX. Besides that, I'll use R for all the research. Some Python can be used too, but I'll try to maintain the consistency and stick to R.

### Who do I talk to? ###

You can contact me at my e-mail marcelogelati@gmail.com. You can check my orienter's site at https://sites.google.com/site/marcelodecgriebeler/home.
